var express = require('express');  // módulo express
var app = express();		   // objeto express
var bodyParser = require('body-parser');  // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var irc = require('irc');
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var proxyObject = require('./proxyEntity.js');
//var cookieParserIO = require('socket.io-cookie');

//Load Hashtable library
Hashtable = require('hashtable');

//Create new Proxies Hashtable
proxyHashtable = new Hashtable();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

//io.use(cookieParser());

var path = require('path');	// módulo usado para lidar com caminhos de arquivos

var proxies = []; // mapa de proxys
var proxy_id = 0;

function proxy(id, servidor, nick, canal, socket)
{
	var cache = []; // cache de mensagens

	var irc_client = new irc.Client(servidor, nick, {channels: [canal],});

	
	irc_client.addListener('motd', function(msg)
	{
//		console.log(msg.toString());
		socket.emit('motd', msg);
	});

	irc_client.addListener('message'+canal, function (from, message)
	{
//		console.log(from + ' => '+ canal +': ' + message);
		cache.push({"timestamp":Date.now(), "nick":from, "msg":message});
		var msg = '['+Date.now()+']'+'<'+from+'>'+message;
		socket.emit('message', msg);
	});	

	irc_client.addListener('error', function(message)
	{
		console.log('ERROR: ', message);
	});

	irc_client.addListener('mode', function(message)
	{
		console.log('mode: ', message);
	});
	
	irc_client.addListener('nick', function (oldnick, newnick, channels, message)
	{
		//console.log('NICK command: oldnick: ' + oldnick + ' => ' + newnick);
		var msg = oldnick + ' is now known as ' + newnick;
		socket.emit('message', msg);
	});

	irc_client.addListener('raw', function(message)
	{
		console.log(message.command);
	});
	
	proxyHashtable.put(nick, proxyObject.constructor(irc_client, cache, socket));
//	proxies[id] = proxyObject.constructor(irc_client, cache, socket);
//	console.log(proxies[id].cache);
	
	return proxyHashtable.get(nick);
}

//Check cookies for login and/or send homepage
app.get('/', function (req, res)
{
	if(req.cookies.servidor && req.cookies.nick  && req.cookies.canal)
	{
		proxy_id++;		
		res.cookie('id', proxy_id);
		res.sendFile(path.join(__dirname, 'web-front/index.html'));
		console.log('Sending homepage');
	}
	else //Send login page
	{
		res.sendFile(path.join(__dirname, 'web-front/login.html'));
	}

});

app.post('/checkNick', function(req, res)
{
	console.log('GOT HERE!!!');
	if(!proxyHashtable.has(req.body.nome))	res.send('OK');
	else res.send('Nick already in use!');
});


//Receive and deal with login info
app.post('/login', function (req, res)
{ 
	res.cookie('nick', req.body.nome);
	res.cookie('canal', req.body.canal);
	res.cookie('servidor', req.body.servidor);
	res.redirect('/');
});

//Put server listening on desired port
server.listen(3000, function()
{
	console.log('Listening on port 3000!');

});
var webScount = 0;
//Open socket between WebClient and Proxy Server
io.on('connection', function(socket)
{
	webScount++;
	console.log('**** ' + webScount + ' connections ****');
	socket.emit('message', "Conectado");
//	console.ilog('I have access: ' + socket.request.headers.cookie);
	
	var re = /\s*;\s*/;	
	var buffer = socket.request.headers.cookie.split(re);
	var id;
	var canal;
	var servidor;
	var nick;

	buffer.forEach(function(data)
	{
		if(data.startsWith('id='))
		{
			id = parseInt(data[3], 10);
			console.log("ID = " + id);
			//proxies[number].irc_client.say('#canal', msg);
		}
		if(data.startsWith('canal='))
		{
			canal = '#'+data.slice(6);
			console.log('CANAL = ' + canal);
		}
		if(data.startsWith('nick='))
		{
			nick = data.slice(5);
			console.log('NICK = ' + nick);
		}
		if(data.startsWith('servidor'))
		{
			servidor = data.slice(9);
			console.log('SERVIDOR = ' + servidor);
		}
	});


	if(proxyHashtable.has(nick)) socket.emit('!nick');
	else var p = proxy(id, servidor, nick, canal, socket);

	socket.on('message', function(msg)
	{
//		console.log(socket.request.headers.cookie);
		var buffer = msg.split(' ');
		if(buffer[0][0] === '/')
			proxyObject.handleCommand(buffer, p);
		else
			p.irc_client.say(canal, msg);
//		console.log('MESSAGE SENT TO IRC_CLIENT');

//		console.log('Message received: ', msg);
	
	});

	socket.on('disconnect', function(reason)
	{
		proxyHashtable.remove(nick);
		socket.disconnect(true);
		console.log('DISCONECTED: ' + reason);
	});
});

