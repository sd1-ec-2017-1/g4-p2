exports.constructor = function(irc_client, cache, socket)
{
	var proxyObject = new Object();
	proxyObject.irc_client = irc_client;
	proxyObject.cache = cache;
	proxyObject.socket = socket;

	return proxyObject;
}

exports.handleCommand = function(buffer, proxyObject)
{
	if(buffer[0] === '/nick')
	{
		proxyObject.irc_client.send('NICK', buffer[1]);
		proxyObject.socket.emit('nick', buffer[1]);
		console.log('GOT HERE!');
	}
}
